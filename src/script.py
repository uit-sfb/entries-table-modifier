from multiprocessing import Pool, cpu_count
import sys, argparse, os
import numpy as np
from datetime import datetime
import json
import urllib3
import math
import shutil
from http.client import responses
from functools import partial, reduce
import collections
import pandas as pd

def main(*argv):
    #format: db_name, x1@y1 ... xn@yn
    args = argv[0]
    if args:
        db_name = args.pop(0).strip()
        tsv_file = args.pop()
        xy_lists = list(map(lambda v: v.split('@'),args))
        partial_func = partial(getRequest,db=db_name)
        xy_dicts = list(map(partial_func,xy_lists))
        one_dict = combineDicts(xy_dicts)
        entries = getTSVInput(tsv_file)
        fillInCountryData(entries,xy_lists,one_dict)
        entries.rename(columns = {"Unnamed: 0" : ""}, inplace = True) #rename index column
        writeToTSV(entries,tsv_file)

def writeToTSV(df,outPath):
    if not df.empty:
        df.to_csv(outPath,sep='\t',index=False)

def addGAZToColumnValues(v,loc_name_dict): #handle cases with multiple values with |
    value = str(v).split('§')
    if len(value) > 0:
        gaz = loc_name_dict.get(value[0].casefold().strip(),[])
        if gaz:
            gaz = list(gaz)
            return "{}§{}".format(v,gaz[0])
    return v

def modifyLocValues(v,loc_name_dict):
    partial_func = partial(addGAZToColumnValues,loc_name_dict=loc_name_dict)
    str_v = str(v)
    if str_v and str_v != "nan":
        value = '|'.join(list(map(partial_func,str(v).split('|'))))
        return value
    return np.nan

def fillInCountryData(df,column_list,loc_name_dict):
    column_pairs = list(filter(lambda t: len(t) == 2,column_list))
    gaz_func = partial(modifyLocValues,loc_name_dict=loc_name_dict)
    for t in column_pairs:
        df[t[0]] = df[t[0]].apply(gaz_func)
        for index, row in df.iterrows():
            row = row.copy()
            row_value = str(row[t[0]])
            if row_value.count('§gaz') == 1:
                if t[1] in df.columns:
                    df.loc[index,t[1]] = row_value

def getTSVInput(input):
    df = pd.read_csv(input,sep='\t',engine='python')
    df.columns = df.columns.str.strip()
    return df

def combineDicts(lst):
    super_dict = collections.defaultdict(set)
    for d in lst:
        for k, v in d.items():
            super_dict[k].add(v)
    return super_dict

def filterGAZValues(lst):
    if len(lst) == 1:
        gaz_id = str(lst[0])
        if "gaz:" in gaz_id.lower():
            return True
    return False

def getGAZDictionary(request,y_val):
    data = json.loads(request.data.decode('UTF-8'))
    del data['graph'][0]
    dicts = list(filter(lambda d: filterGAZValues(d.get(y_val)),data['graph']))
    separatedValues = lambda lst: [lst[0].split('§')[0].casefold().strip(),''.join(lst[1]).lower()]
    res_dict = dict([separatedValues(list(d.values())) for d in dicts]) #dict of x:y_loc
    return res_dict

def getRequest(xy_lst,db):
    x,y = xy_lst
    baseUrl = "https://databasesapi.sfb.uit.no/rpc/v1/{}/graphs".format(db)
    y_val = "y_{}".format(y)
    url = baseUrl + '?' + "x[{}]=eachR".format(x) + '&' + "{}[{}]=setR".format(y_val,y)
    gaz_dict = {}
    count = 0
    completed = False
    http = urllib3.PoolManager()
    while count < 10:
        request = http.request('GET',url)
        http_status = request.status
        http_desc = responses[http_status]
        if http_desc == "OK":
            completed = True
            gaz_dict = getGAZDictionary(request,y_val)
            break
        else:
            print("Request to databaseapi failed with status code: {}, repeating...".format(http_status))
            count = count + 1
    if not completed:
        print("Request to databaseapi failed with status code: {} after 10 attempts".format(http_status))
    return gaz_dict

if __name__ == "__main__":
    main(sys.argv[1:])
