# Entries Table Modifier

Python script which serves as an extension of multifetch. Modify multifetch output .tsv file using data from our databases retrieved via call to databases-api

## Input

Script takes multiple parameters as an input:
 - Database name (for example: `SARS-CoV-2`)
 - Column name for x axis and column name for y axis in the form of x@y pair where "x" is the column with data used as a base to fill in values in empty column "y" (for example: `_intern:loc_name_raw@loc:name`). Number of these pairs is arbitrary.
 - Path to entries.tsv file

 Parameters must be specified in the following order: database name being the first, then the arbitrary number of column name pairs and path to the entries.tsv file being the last. (for example: `SARS-CoV-2 _intern:loc_name_raw@loc:name _intern:loc_country_raw@loc:country ./entries.tsv`)

 ## Output

 Script modifies existing entries.tsv file.
