FROM python:3

LABEL maintainer="mmp@uit.no" \
  toolVersion=0.1 \
  toolName=entries-table-modifier \
  url=https://gitlab.com/uit-sfb/entries-table-modifier

WORKDIR /app/workdir

ENV HOME /app/workdir

RUN chmod g+rwx $HOME

COPY src/requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY /src/*.py ./

ENTRYPOINT [ "python", "./script.py" ] 
